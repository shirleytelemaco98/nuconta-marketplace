import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nuconta_marketplace_app/scr/models/customer_model.dart';
import 'package:nuconta_marketplace_app/scr/models/offer_model.dart';
import 'package:nuconta_marketplace_app/scr/resources/graphql_options.dart';
import 'package:nuconta_marketplace_app/scr/resources/graphql_configuration.dart';
import 'package:nuconta_marketplace_app/scr/ui/home_screen.dart';
import 'package:nuconta_marketplace_app/scr/ui/result_screen.dart';

/// The [ProductScreen] is the screen that shows a product offer detailed. In that
/// is showed the name, image, price and description of the product offer and
/// where is possible finish a purchase for that product showed.
class ProductScreen extends StatelessWidget {
  Customer customer;
  Offer offer;

  ProductScreen (this.customer, this.offer);

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: GraphQLConfiguration().client(),
      child: CacheProvider(
        child: Scaffold (
          appBar: _productAppBar(context),
          body: _mutation(context),
        ),
      ),
    );

    // return Scaffold(
    //   appBar: _productAppBar(context),
    //   body: _productBody(context),
    // );
  }

  PreferredSizeWidget _productAppBar (BuildContext context) {
    return PreferredSize (
      preferredSize: const Size.fromHeight(70),
      child: Align (
        alignment: Alignment.bottomLeft,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: IconButton(
            onPressed:  () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen(customer)),
              );
            },

            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Mutation _mutation (BuildContext context) {
    return Mutation(
        options: MutationOptions (
            document: gql(GraphQLOptions.mutation(offer.id)),
            onCompleted: (result) {
              bool success = result['purchase']['success'];
              String errorMsg =result['purchase']['errorMessage'];
              Customer customer = Customer.fromJson(result['purchase']['customer']);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ResultScreen(customer, success, errorMsg), )
              );
            },
        ),
        builder: ( RunMutation runMutation, QueryResult result ) {
          return Padding (
            padding: const EdgeInsets.all(15),
            child: ListView(
              shrinkWrap: true,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    offer.product.name,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.white,
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 25),
                  child: Container(
                    color: Colors.white,
                    child: Image.network(
                      offer.product.image,
                      height: 300,
                    ),
                  ),
                ),

                Text(
                  "R\$ " + offer.price.toString(),
                  style: const TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w700,
                    color: Colors.greenAccent,
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 50,),
                  child: Text(
                    offer.product.description,
                    textAlign: TextAlign.justify,
                    style: const TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),

                OutlinedButton (
                  onPressed: () => runMutation({ }),
                  style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                    side: MaterialStateProperty.all<BorderSide> (
                        const BorderSide(
                          color: Colors.white,
                          width: 2,
                        )
                    ),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Purchase',
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
    );
  }
}