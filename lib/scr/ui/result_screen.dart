import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nuconta_marketplace_app/scr/models/customer_model.dart';
import 'package:nuconta_marketplace_app/scr/ui/home_screen.dart';
import 'package:splashscreen/splashscreen.dart';

/// The [ResultScreen] is the class responsible to show the result of a product
/// purchase, if it was successful or not and the reason
class ResultScreen extends StatefulWidget {
  Customer customer;
  bool success;
  String? errorMessage;

  ResultScreen (this.customer, this.success, this.errorMessage);

  @override
  State<StatefulWidget> createState() {
    return ResultState(customer, success, errorMessage);
  }
}

class ResultState extends State<ResultScreen> {
  Customer customer;
  bool success;
  String? errorMessage;

  ResultState (this.customer, this.success, this.errorMessage);

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      // 5s over, navigate to a new page
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => HomeScreen(customer)));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      body: _body (context),
    );
  }

  Widget _body (BuildContext context) {
    return Center(
      child: Container(
        color: Colors.white,
        width: double.infinity,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (success)
              const Icon(
                Icons.check_circle,
                color: Colors.green,
                size: 200,
              ),
            if (!success)
              const Icon(
                Icons.warning,
                color: Colors.red,
                size: 100,
              ),

            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Text(
                success ? 'Thank you for your purchase!' : errorMessage!,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontFamily: 'Arial',
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}