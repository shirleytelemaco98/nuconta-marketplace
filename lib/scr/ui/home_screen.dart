import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nuconta_marketplace_app/scr/models/customer_model.dart';
import 'package:nuconta_marketplace_app/scr/ui/components/account_card.dart';
import 'package:nuconta_marketplace_app/scr/ui/components/offer_card.dart';
import 'package:nuconta_marketplace_app/scr/utils/nu_colors.dart';

/// The [HomeScreen] is the class that is responsible to show the [customer]
/// information and the offers available to it.
class HomeScreen extends StatelessWidget {
  Customer customer;
  HomeScreen (this.customer);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _homeAppBar(),
      body: _homeBody(context),
    );
  }

  PreferredSizeWidget _homeAppBar () {
    return PreferredSize (
      preferredSize: const Size.fromHeight(100),
      child: Container (
        height: 100,
        color: NuColors.bgColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 20, 10, 0),
              child: Row(
                children: <Widget> [
                  Expanded(
                    child: Text(
                      'Hi, ' + customer.name + '!',
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _homeBody (BuildContext context) {
    return Container(
      color: NuColors.bgColor,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
        child: ListView(
          shrinkWrap: true,
          children: <Widget> [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                AccountCard.accountCard(
                  icon: Icons.attach_money_outlined,
                  title: 'Account',
                  description: 'Balance available',
                  value: customer.balance,
                  valueColor: Colors.blueAccent,
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Row(
                    children: <Widget> [
                      Expanded(
                        child: Container (
                          color: Colors.white,
                          width: double.infinity,
                          height: 1,
                        ),
                      ),

                      const Text (
                        ' Offers ',
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),

                      Expanded(
                        child: Container (
                          color: Colors.white,
                          width: double.infinity,
                          height: 1,
                        ),
                      ),
                    ],
                  ),
                ),

                GridView.count(
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.vertical,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 2,
                  childAspectRatio: .85,

                  children: <Widget> [
                    for (var offer in customer.offers)
                      OfferCard.offerCard(context, customer, offer, offer.product.name, offer.price, offer.product.image),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}