import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nuconta_marketplace_app/scr/models/customer_model.dart';
import 'package:nuconta_marketplace_app/scr/models/offer_model.dart';
import 'package:nuconta_marketplace_app/scr/ui/product_screen.dart';
import 'package:nuconta_marketplace_app/scr/utils/nu_colors.dart';

/// The [OfferCard] class constructs a card with an product information
class OfferCard {
  static Container offerCard (BuildContext context, Customer customer, Offer offer, String productName, int price, String imageUrl) {
    return Container (
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 0.0,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(3.0),
        ),
      ),

      child: Column(
        children: [
          Image.network(imageUrl, height: 80,),

          Padding(
            padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
            child: Column(
              children: [
                Text(
                  productName,
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                  ),
                ),

                const Padding(padding: EdgeInsets.only(top: 15)),

                Text(
                  'R\$ $price',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueAccent,
                  ),
                ),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    OutlinedButton (
                      onPressed:  () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ProductScreen(customer, offer)),
                        );
                      },
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(NuColors.bgColor),
                      ),
                      child: const Text('Purchase'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}