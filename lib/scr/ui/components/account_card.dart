import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nuconta_marketplace_app/scr/utils/nu_colors.dart';

/// The [AccountCard] class constructs a card with the information of the Customer accountant.
class AccountCard {
  static Container accountCard({IconData? icon, title, subtitle, description, value, Color? valueColor = Colors.black, buttonText, footer}) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(30, 15, 30, 25),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 0.0,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(3.0),
        ),
      ),

      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          children: <Widget> [
            if (title != null) Row(
              children: <Widget> [
                Icon(
                  icon, // Icon
                  color: Colors.grey[500],
                ),
                const Padding(padding: EdgeInsets.fromLTRB(0, 0, 15, 0)),

                Text(
                  '$title',
                  style: const TextStyle(
                    color: Colors.black54,
                  ),
                ),
              ],
            ),

            if (subtitle != null) Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Text(
                    '$subtitle',
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                )
            ),

            if (description != null) Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                child: Text(
                  '$description',
                  textAlign: TextAlign.left,
                ),
              ),
            ),

            if (value != null) Align(
              alignment: Alignment.centerLeft,
              child: Padding (
                padding: const EdgeInsets.fromLTRB(0, 15, 0, 5),
                child: Text(
                  'R\$ $value',
                  textAlign: TextAlign.left,
                  style: TextStyle (
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: valueColor,
                  ),
                ),
              ),
            ),

            if (buttonText != null) Align(
              alignment: Alignment.centerLeft,
              child: OutlinedButton(
                onPressed: () {  }, // TODO
                child: Text(
                  buttonText,
                  style: TextStyle(
                    color: NuColors.bgColor,
                    fontSize: 12,
                  ),
                ),

              ),
            ),

            if (footer != null) Align(
              alignment: Alignment.centerLeft,
              child: Padding (
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Text(
                  '$footer',
                  textAlign: TextAlign.left,
                  style: const TextStyle (
                    fontSize: 12,
                    // color: valueColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}