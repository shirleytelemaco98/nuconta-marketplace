import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nuconta_marketplace_app/scr/models/customer_model.dart';
import 'package:nuconta_marketplace_app/scr/resources/graphql_options.dart';
import 'package:nuconta_marketplace_app/scr/ui/home_screen.dart';
import 'package:nuconta_marketplace_app/scr/utils/nu_colors.dart';
import 'package:nuconta_marketplace_app/scr/utils/nu_strings.dart';
import 'package:splashscreen/splashscreen.dart';

/// The [NuSplashScreen] is the first screen of the app. It is responsible to
/// introduce the user to the app and to retrieve the initial data, at this case,
/// all the information about the present customer.
class NuSplashScreen {
  late final Customer customer;
  ValueNotifier<GraphQLClient> client;
  NuSplashScreen (this.client);

  Widget home (BuildContext context) {

    return GraphQLProvider(
      client: client,
      child: CacheProvider(
        child: MaterialApp (
          theme: ThemeData (
            scaffoldBackgroundColor: NuColors.bgColor,
          ),
          home: _splashBody(context),
        ),
      ),
    );
  }

  Widget _splashBody (BuildContext context) {
    return Query(
      options: QueryOptions(
        document: gql(GraphQLOptions().queryRead),
      ),

      builder: (
          QueryResult result, {
            Refetch? refetch,
            FetchMore? fetchMore,
        }) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }

          if (result.isLoading) {
            return const Text('Loading');
          }

          if (result.data == null) {
            return const Center(
              child: Text(
                "Loading...",
                style: TextStyle(fontSize: 20.0),
              ));
          } else {

            Customer customer = Customer.fromJson(result.data['viewer']);

            return Stack(
              children: [
                SplashScreen(
                  seconds: 5,
                  backgroundColor: NuColors.bgColor,
                  loaderColor: Colors.transparent,
                  navigateAfterSeconds: HomeScreen(customer),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget> [
                    Center(
                      child: Container(
                        height: 150,
                        width: 150,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/images/nu_icon.png'),
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            );
          }
      },
    );
  }
}