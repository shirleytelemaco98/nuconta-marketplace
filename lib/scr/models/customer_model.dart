import 'package:json_annotation/json_annotation.dart';
import 'package:nuconta_marketplace_app/scr/models/offer_model.dart';

part 'g/customer_model.g.dart';

/// The [Customer] class is used to refer to the bank client person.
///
/// Customer class is composed by id, name, balance and offers fields.
/// Where the [id] represents the value of identification of a customer,
/// the [name] represents the customer name,
/// the [balance] represents the money value of the customer accountant,
/// and the [offers] represent offers available for a customer purchase.
@JsonSerializable()
class Customer {
  String id;
  String name;
  int balance;
  List<Offer> offers;

  /// Customer parametrized constructor
  Customer (this.id, this.name, this.balance, this.offers);

  /// Constructs a customer object since a json instance
  factory Customer.fromJson(Map<String, dynamic> json) => _$CustomerFromJson(json);

  /// Returns the customer object in json format
  Map<String, dynamic> toJson() => _$CustomerToJson(this);
}