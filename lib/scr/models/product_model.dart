import 'package:json_annotation/json_annotation.dart';

part 'g/product_model.g.dart';

/// The [Product] class is used to refer a marketplace product.
///
/// Product class is composed by id, name, description and image fields.
/// Where the [id] represents the value of identification of an product,
/// the [name] represents the name of the product,
/// the [description] represents the product description
/// and the [image] represent the product image.
@JsonSerializable()
class Product {
  String id;
  String name;
  String description;
  String image;

  /// Product parametrized constructor
  Product(this.id, this.name, this.description, this.image);

  /// Constructs a product object since a json instance
  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);

  /// Returns the product object in json format
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}