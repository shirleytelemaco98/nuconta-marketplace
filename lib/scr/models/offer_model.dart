import 'package:json_annotation/json_annotation.dart';
import 'package:nuconta_marketplace_app/scr/models/product_model.dart';

part 'g/offer_model.g.dart';

/// The [Offer] class is used to refer a offer of a marketplace product.
///
/// Offer class is composed by id, price and product fields.
/// Where the [id] represents the value of identification of an offer,
/// the [price] represents the value of the offer
/// and the [product] represent a product offer.
@JsonSerializable()
class Offer {
  String id;
  int price;
  Product product;

  /// Offer parametrized constructor
  Offer (this.id, this.price, this.product);

  /// Constructs a offer object since a json instance
  factory Offer.fromJson(Map<String, dynamic> json) => _$OfferFromJson(json);

  /// Returns the Offer object in json format
  Map<String, dynamic> toJson() => _$OfferToJson(this);
}