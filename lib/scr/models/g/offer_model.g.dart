// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../offer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Offer _$OfferFromJson(Map<String, dynamic> json) {
  return Offer(
    json['id'] as String,
    json['price'] as int,
    Product.fromJson(json['product'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OfferToJson(Offer instance) => <String, dynamic>{
      'id': instance.id,
      'description': instance.price,
      'product': instance.product,
    };
