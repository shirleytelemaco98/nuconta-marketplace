class NuStrings {
  /// The endpoint link address
  static String httpLink = "https://staging-nu-needful-things.nubank.com.br/query";
  /// The bearerToken to authenticate on requisition
  static String bearerToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhd2Vzb21lY3VzdG9tZXJAZ21haWwuY29tIn0.cGT2KqtmT8KNIJhyww3T8fAzUsCD5_vxuHl5WbXtp8c";
}