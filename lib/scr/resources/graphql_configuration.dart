import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nuconta_marketplace_app/scr/utils/nu_strings.dart';

/// The [GraphQLConfiguration] class aims the create the structure to the GraphQL client
class GraphQLConfiguration {
  ValueNotifier<GraphQLClient> client () {

    final HttpLink httpLink = HttpLink(
      NuStrings.httpLink,
    );

    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer ' + NuStrings.bearerToken,
    );

    final Link link = authLink.concat(httpLink);

    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        link: link,
        cache: GraphQLCache(store: HiveStore()),
      ),
    );

    return client;
  }
}