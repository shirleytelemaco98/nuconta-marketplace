
/// The [GraphQLOptions] class aims to generate the GraphQL query and mutations options
class GraphQLOptions {
  String queryRead = """ 
    query {
      viewer {
        id
        name
        balance
        offers {
          id
          price
          product {
            id
            name
            description
            image
          }
        }
      }
    }
   """;

  static String mutation(String offerID) {
    return """
      mutation {
        purchase (offerId: \"""" + offerID + """\") {
          success
          errorMessage
          customer {
            id
            name
            balance
            offers {
              id
              price
              product {
                id
                name
                description
                image
              }
            }
          }
        }
      } 
    """;
  }
}