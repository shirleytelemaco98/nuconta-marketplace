import 'package:flutter/material.dart';
import 'package:graphql/src/graphql_client.dart';
import 'package:nuconta_marketplace_app/scr/ui/splash_screen.dart';

class NuMarketplaceApp extends StatelessWidget {
  ValueNotifier<GraphQLClient> client;

  NuMarketplaceApp (this.client, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'NuConta Makertplace',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: HomePage(client, title: 'Marketplace'),
    );
  }
}

class HomePage extends StatefulWidget {
  final String title;
  ValueNotifier<GraphQLClient> client;

  HomePage(this.client, {Key? key, required this.title}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState(client);
}

class _HomePageState extends State<HomePage> {
  ValueNotifier<GraphQLClient> client;
  _HomePageState(this.client) : super();

  @override
  Widget build(BuildContext context) {
    return (NuSplashScreen(client)).home(context);
  }
}