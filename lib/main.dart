import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nuconta_marketplace_app/scr/app.dart';
import 'package:nuconta_marketplace_app/scr/resources/graphql_configuration.dart';

void main() async {
  await initHiveForFlutter();

  runApp(NuMarketplaceApp(GraphQLConfiguration().client()));
}