@echo off
rem This file was created by pub v2.14.0-136.0.dev.
rem Package: dartdoc
rem Version: 0.45.0
rem Executable: dartdoc
rem Script: dartdoc
if exist "C:\Users\sfreitas\AppData\Local\Pub\Cache\global_packages\dartdoc\bin\dartdoc.dart-2.14.0-136.0.dev.snapshot" (
  dart "C:\Users\sfreitas\AppData\Local\Pub\Cache\global_packages\dartdoc\bin\dartdoc.dart-2.14.0-136.0.dev.snapshot" %*
  rem The VM exits with code 253 if the snapshot version is out-of-date.
  rem If it is, we need to delete it and run "pub global" manually.
  if not errorlevel 253 (
    goto error
  )
  pub global run dartdoc:dartdoc %*
) else (
  pub global run dartdoc:dartdoc %*
)
goto eof
:error
exit /b %errorlevel%
:eof

