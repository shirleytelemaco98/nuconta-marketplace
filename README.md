# NuConta Marketplace
## _The app to spent the NuConta balance on products_

Nu-Marketplace is an application developed in Flutter - Dart and is compatible with Android and iOS.

- See a product
- Purchase
- ✨Magic ✨

## Features

- See the product offers available
- Make a purchase of a product using the accountant balance

About NuBank...

> Nubank is a Latin American neobank and the 
> largest financial technology bank in Latin America. 
> Its headquarters are located in São Paulo, Brazil. 
> The company also has an engineering office in Berlin, 
> Germany, and an office in Mexico City, Mexico.

We are a startup developing simple, secure, and 100% digital solutions so you can have control over your money, literally in the palm of your hands. And with the NuConta Makertplace app you can enjoy of all offers using just two clicks.

## Tech

Dillinger uses a number of open source projects to work properly:

- [Dart] - programming language designed for client side
- [Flutter] - open-source UI software development kit
- [GraphQL] - an open-source data query and manipulation language for APIs

You can find the Nu-Marketplace in [this repository][numkt] on GitLab.

## Installation

Nu-Marketplace requires [Flutter] v2.2+ to run.

Install the dependencies and devDependencies and start develop the app.

 - Through the  [Flutter Tutorial](https://flutter.dev/docs/get-started/install) you can install the Flutter dependences on your machine according with your Operational System.
 - It was used the [Android Studio](https://developer.android.com/studio) platform to structure and develop the app

**Nu new app, Hell Yeah!**

[//]: #
   [numkt]: <https://gitlab.com/shirleytelemaco98/nuconta-marketplace>
   [Dart]: <https://graphql.org>
   [Flutter]: <https://flutter.dev>
   [GraphQL]: <https://dart.dev>
